$(document).ready(function () {
   //initialize swiper when document ready
   var mySwiper = new Swiper ('.swiper-container', {

     effect: 'fade',

    // Keyboard Navigation
    keyboard: {
      enabled: true,
      onlyInViewport: false,
    },

  });

  // MENU
  $('#openmenu').click(function() {
    $(this).toggleClass('open');
    $('#menu').toggleClass('show');
  });
  $('#menu li').click(function() {
    $('#menu').removeClass('show');
    $('#openmenu').removeClass('open');
  });
  $('#inicio').click(function() {
    mySwiper.slideTo(0);
  });

});
